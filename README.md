Cold to Frozen S3 Add-on
========================

This is a Splunk add-on based on Atlassian's cold to frozen S3 script, a helper utility for rolling your frozen buckets to Amazon's S3 archival service.

This lets you avoid losing data permanently, without need to scale out costly EBS volumes infinitely to meet your offline retention requirements.

Usage
=====

Ensure your Splunk indexers are launched with an instance profile which permits uploading to the S3 bucket.

      {
          "Version": "2012-10-17",
          "Statement": [
              {
                  "Effect": "Allow",
                  "Action": [
                      "s3:ListBucket"
                  ],
                  "Resource": [
                      "arn:aws:s3:::example-bucket"
                  ]
              },
              {
                  "Effect": "Allow",
                  "Action": [
                      "s3:PutObject",
                      "s3:GetObject"
                  ],
                  "Resource": [
                      "arn:aws:s3:::example-bucket/*"
                  ]
              }
          ]
      }

NOTE: It is recommended to use purely IAM roles versus hard-coding IAM user credentials to interact with S3.  The script will still allow you to configure the S3 credentials externally using environment variables or config files if you need.

Configure your S3 details in **atlassian-add-on-cold-to-frozen-s3/local/cold_to_frozen_s3.conf**

      [atlassian_cold_to_frozen_s3]
      region = ap-southeast-2
      bucket_name = example-bucket

In your **indexes.conf** configure each index to use the coldToFrozenS3.py script

      [example]
      # Upload buckets to S3 rather than deleting them directly
      coldToFrozenScript = /usr/bin/python /opt/splunk/etc/apps/cold-to-frozen-s3/bin/coldToFrozenS3.py
      # 7 days for example data
      frozenTimePeriodInSecs = 604800

# Limitations

* S3 buckets must already exist to limit the scope needed for the script.


Installation
============

Install this by placing the repo contents into your Splunk server's `etc/apps` directory.

Edit the `indexes.conf` as mentioned above, and then restart Splunk to pick up the new settings.

For example:

      my-splunk-indexer in /opt/splunk/etc/apps
      ○ → git clone git@bitbucket.org:asecurityteam/atlassian-add-on-cold-to-frozen-s3.git

      <... edit indexes.conf ...>

      ○ → /opt/splunk/bin/splunk restart

# NOTE
* If your deployment is distributed, you'll want to deploy the add-on primarily on the indexers
* Make sure to trial the script on a single, "test" index in your `indexes.conf` file before setting globally and/or on large production indexes (you can easily blackhole data if you're not careful)

Documentation
=============

All docs are located on BitBucket at https://bitbucket.org/asecurityteam/atlassian-add-on-cold-to-frozen-s3

Tests
=====

    to do

Contributors
============

Pull requests, issues and comments welcome. For pull requests:

* Add tests for new features and bug fixes
* Follow the existing style
* Separate unrelated changes into multiple pull requests

See the existing issues for things to start contributing.

For bigger changes, make sure you start a discussion first by creating
an issue and explaining the intended change.

Atlassian requires contributors to sign a Contributor License Agreement,
known as a CLA. This serves as a record stating that the contributor is
entitled to contribute the code/documentation/translation to the project
and is willing to have it used in distributions and derivative works
(or is willing to transfer ownership).

* [CLA for corporate contributors](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)
* [CLA for individuals](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)

License
========

Copyright (c) 2016 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.
