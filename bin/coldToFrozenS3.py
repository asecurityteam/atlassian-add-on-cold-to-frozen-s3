#!/usr/bin/env python3
__author__  = "Atlassian"

import sys
import os
import subprocess

BUCKET="security-splunk-index-archives"

class ColdToFrozenS3Error(Exception): pass

def main():

    if len(sys.argv) < 2:
        sys.exit('usage: python coldToFrozenS3.py <bucket_dir_to_archive>')

    colddb = sys.argv[1]

    if colddb.endswith('/'):
        colddb = colddb[:-1]

    if not os.path.isdir(colddb):
        sys.exit("Provided path is not directory: " + colddb)

    rawdir = os.path.join(colddb,'rawdata')
    if not os.path.isdir(rawdir):
        sys.exit("No rawdata directory, this is probably not an index database: " + colddb)

    # ok, we have a path like this: /SPLUNK/DB/PATH/$INDEX/colddb/db_1452411618_1452411078_1403
    # and we want it to end up like this:
    # s3://BUCKETNAME/$INDEX/frozendb/db_1452411618_1452411078_1403

    segments = colddb.rsplit('/',3)
    if len(segments) != 4:
        sys.exit("Path broke into incorrect segments: " + segments)
    # should be like: ['/SPLUNK/DB/PATH', '$INDEX', 'colddb', 'db_1452411618_1452411078_1403']

    remotepath = 's3://{s3bucket}/{index}/frozendb/{splunkbucket}'.format(s3bucket=BUCKET,index=segments[1],splunkbucket=segments[3])

    s3args = 'sync ' + colddb + ' ' + remotepath

    command = '/usr/local/bin/aws s3 ' + s3args
    command = command.split(' ')

    try:
        del os.environ['LD_LIBRARY_PATH']
        del os.environ['PYTHONPATH']
    except KeyError:
        pass

    try:
        # Benchmarked our indexers being able to upload 10GB to S3 in us-west-2 usualy ~5mins
        # We kill uploads taking longer than 15 minutes to free up Splunk worker slots
        awscli = subprocess.check_call(command,
				     stdout=sys.stdout,
				     stderr=sys.stderr,
				     timeout=900)
    except subprocess.TimeoutExpired:
        raise ColdToFrozenS3Error("S3 upload timedout and was killed")
    except:
        raise ColdToFrozenS3Error("Failed executing AWS CLI")

    print('Froze {0} OK'.format(sys.argv[1]))

main()
